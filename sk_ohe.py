import pandas as pd
from sklearn.preprocessing import OneHotEncoder

ohe = OneHotEncoder(handle_unknown='ignore')
data_train = pd.DataFrame({'职业': ['数据挖掘工程师', '数据库开发工程师', '数据分析师', '数据分析师'],
                           '籍贯': ['福州', '厦门', '泉州', '龙岩']})
ohe.fit(data_train)  # 训练规则
feature_names = ohe.get_feature_names(data_train.columns)  # 获取编码后的特征名
data_train_onehot = pd.DataFrame(ohe.transform(data_train).toarray(), columns=feature_names)  # 应用规则在训练集上

print(data_train_onehot)

data_new = pd.DataFrame({'职业': ['数据挖掘工程师', 'jave工程师'],
                         '籍贯': ['福州', '莆田']})
data_new_onehot = pd.DataFrame(ohe.transform(data_new).toarray(), columns=feature_names)  # 应用规则在预测集上
print(data_new_onehot)


#编码与哑变量
#将分类转换为分类数值   LabelEncoder标签专用，所以不需要是矩阵
from sklearn.preprocessing import LabelEncoder
y = data.iloc[:,-1] #想看y有多少种使用set(y)即可  有三种，另外一个是Unknow
enc = LabelEncoder()
label = enc.fit_transform(y)
data.iloc[:,-1]=label
enc.classes_  #查看有多少种类别