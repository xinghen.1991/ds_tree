import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction import DictVectorizer
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import classification_report
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier


#通过pandas读取网上的泰坦尼克号数据
titanic = pd.read_csv('http://biostat.mc.vanderbilt.edu/wiki/pub/Main/DataSets/titanic.txt')
#观察前几行数据
print(titanic.head())
#titanic是dataframe格式，因此可以使用info()方法查看数据的统计特性
print(titanic.info())

#选择三个重要的特征
X = titanic[['pclass', 'age', 'sex']]
y = titanic['survived']

#查看X的数据信息
print(X.info())  #发现age中有600多个NaN值，sex和pclass是类别变量需要转换为特征数值
#用平均数补充age中的缺失值
X['age'].fillna(X['age'].mean(), inplace=True)
print(X.info())  #确认age特征得到了补充

#使用feature_extraction中的特征转换器，把类别变量中的特征都单独剥离出来，独立成一列特征
vec = DictVectorizer(sparse=False)
X = vec.fit_transform(X.to_dict(orient='record'))
print(vec.feature_names_)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=2)

#使用单一决策树进行模型训练和预测分析
dtc = DecisionTreeClassifier()
dtc.fit(X_train, y_train)
y_dtc_predict = dtc.predict(X_test)

#使用随机森林分类器进行模型训练和预测分析
rfc = RandomForestClassifier()
rfc.fit(X_train, y_train)
y_rfc_predict = rfc.predict(X_test)

#使用梯度上升决策树分类器进行模型训练和预测分析
gbc = GradientBoostingClassifier()
gbc.fit(X_train, y_train)
y_gbc_predict = gbc.predict(X_train)

#输出三种分类器的预测结果
print('The Accuracy of DecisionTreeClassifier is:', dtc.score(X_test, y_test))
print('The Accuracy of RandomForestClassifier is:', rfc.score(X_test, y_test))
print('The Accuracy of GradientBoosstingClassifier is:', gbc.score(X_test, y_test))