import json
import lightgbm as lgb
import pandas as pd
from sklearn.metrics import mean_squared_error
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.datasets import make_classification
from category_encoders import OrdinalEncoder
from category_encoders import OneHotEncoder

# 加载你的数据
print('Load data...')
df_train = pd.read_csv('data.csv').fillna("0")
df_train = df_train.set_index("井名")
df_result = pd.read_csv('result1.csv')

X_train, X_test, y_train, y_test = train_test_split(df_train, df_result, test_size=0.33, random_state=42)

y_train = y_train.values
y_test = y_test.values
X_train = X_train.values
X_test = X_test.values

encoder = OneHotEncoder(cols=[0, 1, 3, 7, 8, 9, 10, 11, 12],
                        handle_unknown='indicator',
                        handle_missing='indicator',
                        use_cat_names=True)  # 在训练集上训练

encoded_train_X = encoder.fit_transform(X_train, y_train)  # 转换训练集
encoded_test_X = encoder.fit_transform(X_test, y_test) # 转换测试集


# 将 handle_unknown设为‘indicator’，即会新增一列指示未知特征值
# 将 handle_missing设为‘indicator’，即会新增一列指示缺失值
# 其他的handle_unknown/handle_missing 的选择为：
# ‘error’：即报错; ‘return_nan’：即未知值/缺失之被标记为nan; ‘value’：即未知值/缺失之被标记为0


# 创建成lgb特征的数据集格式
lgb_train = lgb.Dataset(encoded_train_X, y_train)  # 将数据保存到LightGBM二进制文件将使加载更快
lgb_eval = lgb.Dataset(encoded_test_X, y_test, reference=lgb_train)  # 创建验证数据

# 将参数写成字典下形式
params = {
    'task': 'train',
    'boosting_type': 'gbdt',  # 设置提升类型
    'objective': 'regression',  # 目标函数
    'metric': {'l2', 'auc'},  # 评估函数
    'num_leaves': 31,  # 叶子节点数
    'learning_rate': 0.05,  # 学习速率
    'feature_fraction': 0.9,  # 建树的特征选择比例
    'bagging_fraction': 0.8,  # 建树的样本采样比例
    'bagging_freq': 5,  # k 意味着每 k 次迭代执行bagging
    'verbose': 1  # <0 显示致命的, =0 显示错误 (警告), >0 显示信息
}

print('Start training...')
# 训练 cv and train
gbm = lgb.train(params, lgb_train, num_boost_round=20, valid_sets=lgb_eval, early_stopping_rounds=5)  # 训练数据需要参数列表和数据集

print('Save model...')

gbm.save_model('model.txt')  # 训练后保存模型到文件

print('Start predicting...')
# 预测数据集
y_pred = gbm.predict(X_test, num_iteration=gbm.best_iteration)  # 如果在训练期间启用了早期停止，可以通过best_iteration方式从最佳迭代中获得预测
# 评估模型
print('The rmse of prediction is:', mean_squared_error(y_test, y_pred) ** 0.5)  # 计算真实值和预测值之间的均方根误差
